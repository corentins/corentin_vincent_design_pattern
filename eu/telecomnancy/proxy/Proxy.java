package eu.telecomnancy.proxy;


import java.util.Date;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class Proxy extends AbstractSensor{
	
	protected AbstractSensor sensor;
	protected SensorLogger sensorLogger= new SimpleSensorLogger();
	
	public Proxy(AbstractSensor sensor){
		this.sensor=sensor;	
	}
	
	public void on() {
		Date date = new Date();
		this.sensor.on();
		sensorLogger.log(LogLevel.INFO,date+" m�thode on de type void");
	}
	
	@Override
	public void off() {
		Date date = new Date();
		this.sensor.off();
		sensorLogger.log(LogLevel.INFO,date+" m�thode off de type de void");
		
	}

	@Override
	public boolean getStatus() {
		Date date = new Date();
		sensorLogger.log(LogLevel.INFO,date+" m�thode getStatus de type boolean");
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		Date date = new Date();
		sensorLogger.log(LogLevel.INFO,date+" m�thode update de type void");
		this.sensor.update();
		notifyObservateur();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		Date date = new Date();
		sensorLogger.log(LogLevel.INFO,date+" m�thode getValue de type double");
		return this.sensor.getValue();
	}

}
