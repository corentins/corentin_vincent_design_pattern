package eu.telecomnancy;

import java.io.IOException;

import eu.telecomnancy.proxy.Proxy;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorFactory;
import eu.telecomnancy.ui.MainWindow;


public class SwingApp {

    public static void main(String[] args) throws IOException {
    
    	AbstractSensor sensor = SensorFactory.getSensor("TemperatureSensor");
    	AbstractSensor sensor2= SensorFactory.getSensor("StateSensor");
    	AbstractSensor sensor3= SensorFactory.getSensor("LegacyTemperatureSensor");
        
        AbstractSensor proxy = new Proxy(sensor);   
        
        new MainWindow(proxy);
    }

}
