package eu.telecomnancy.observer;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface IObservable {
	
	public void addObservateur(IObservateur o);
    
    public void delObservateur(IObservateur o);
    
    public void notifyObservateur() throws SensorNotActivatedException;

}
