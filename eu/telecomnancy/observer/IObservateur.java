package eu.telecomnancy.observer;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface IObservateur {
	
	public void update() throws SensorNotActivatedException;

}
