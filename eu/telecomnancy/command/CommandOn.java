package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;

public class CommandOn implements ICommand{
	
	private AbstractSensor sensor;
	
	public CommandOn(AbstractSensor sensor){
		this.sensor=sensor;
	
	}
	@Override
	public void execute() {
		sensor.on();
	}

}
