package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandGetValue implements ICommand{
	
	private AbstractSensor sensor;
	
	public CommandGetValue(AbstractSensor sensor) throws SensorNotActivatedException{
		this.sensor=sensor;
		
	}

	public void execute() {
		try {
			sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
