package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandUpdate implements ICommand{
	
		private AbstractSensor sensor;
	
		public CommandUpdate(AbstractSensor sensor){
			
			this.sensor=sensor;
		}
			
		public void execute(){
			
			try {
				sensor.update();
			} catch (SensorNotActivatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}

}
