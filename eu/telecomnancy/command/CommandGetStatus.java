package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;

public class CommandGetStatus implements ICommand{
	
	private AbstractSensor sensor;
	
	public CommandGetStatus(AbstractSensor sensor){
		this.sensor=sensor;
		
	}

	public void execute() {
		sensor.getStatus();
	}

}
