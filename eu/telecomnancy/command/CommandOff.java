package eu.telecomnancy.command;

import eu.telecomnancy.sensor.AbstractSensor;

public class CommandOff implements ICommand{
	
	private AbstractSensor sensor;
	
	public CommandOff(AbstractSensor sensor){
		this.sensor=sensor;
	}
	@Override
	public void execute() {
		sensor.off();
	}

}
