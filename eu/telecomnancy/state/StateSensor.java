package eu.telecomnancy.state;

import java.util.Random;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateSensor extends AbstractSensor{

	private double value;
	
	private Context context;

	public StateSensor(){
		this.context=new Context();
	}
	@Override
	public void on() {
		IState on = new On();
		on.execute(context);
		
	}

	@Override
	public void off() {
		IState off = new Off();
		off.execute(context);
		
	}

	@Override
	public boolean getStatus() {
		
		return context.getState() instanceof On; //permet de v�rifier si une classe est un instance de la classe 
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (getStatus()){
            value = (new Random()).nextDouble() * 100;
        	notifyObservateur();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		 if (getStatus())
	            return value;
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	
	
}
