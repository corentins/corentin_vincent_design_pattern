package eu.telecomnancy.state;


public interface IState {

	public void execute(Context c);
}
