package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.state.StateSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
    	AbstractSensor sensor = new TemperatureSensor();
    	AbstractSensor sensor2 = new StateSensor();
        new ConsoleUI(sensor);
        new ConsoleUI(sensor2);
    }

}