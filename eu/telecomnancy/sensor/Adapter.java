package eu.telecomnancy.sensor;


public class Adapter extends AbstractSensor{
	
	
	LegacyTemperatureSensor oldSensor = new LegacyTemperatureSensor();
	@Override
	public void on() {
		// TODO Auto-generated method stub
		if(oldSensor.getStatus()==false)
			oldSensor.onOff();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		if(oldSensor.getStatus()==true)
			oldSensor.onOff();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return oldSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		  oldSensor.onOff();
		  oldSensor.onOff();
		  notifyObservateur();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return oldSensor.getTemperature();
	}
}
