package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.observer.IObservable;
import eu.telecomnancy.observer.IObservateur;


public abstract class AbstractSensor implements ISensor,IObservable{
	
	 private ArrayList<IObservateur> listObservateur = new ArrayList<IObservateur>();
	
	 @Override
		public void addObservateur(IObservateur obs) {

			listObservateur.add(obs);
		}

		@Override
		public void delObservateur(IObservateur obs) {
			
			listObservateur.remove(obs);
		}

		@Override
		public void notifyObservateur() throws SensorNotActivatedException {
			
			for(int i=0;i<listObservateur.size();i++)
			{
				IObservateur obs = listObservateur.get(i);
				obs.update();
			}
		}

}
