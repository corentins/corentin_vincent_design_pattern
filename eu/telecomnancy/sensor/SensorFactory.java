package eu.telecomnancy.sensor;

import eu.telecomnancy.proxy.Proxy;
import eu.telecomnancy.state.StateSensor;

public class SensorFactory {
	
	private static AbstractSensor sensor;
	
	public static AbstractSensor getSensor(String sensorType)
	{
		if(sensorType.equals("TemperatureSensor")){
			return new TemperatureSensor();
		}
		else if (sensorType.equals("LegacyTemperatureSensor")){
			return new Adapter();
		}
		else if (sensorType.equals("StateSensor")){
			return new StateSensor();
		}
		return null;
		
	}
}
