package eu.telecomnancy.sensor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import eu.telecomnancy.command.CommandGetValue;
import eu.telecomnancy.command.CommandOff;
import eu.telecomnancy.command.CommandOn;
import eu.telecomnancy.command.CommandUpdate;
import eu.telecomnancy.command.ICommand;
import eu.telecomnancy.command.Invoker;
import eu.telecomnancy.helpers.ReadPropertyFile;


public class Menu extends JMenuBar{
	
	private JMenu menu = new JMenu();
	private AbstractSensor sensor;
	
	public Menu(AbstractSensor sensor) throws IOException{
		
		this.sensor=sensor;
		menu.setText("Command List");
		ReadPropertyFile rp=new ReadPropertyFile();
	    Properties p=rp.readFile("/eu/telecomnancy/commande.properties");
	    
	    JMenuItem item[] = new JMenuItem[p.size()];
	    
	    for(int k=0;k<p.size();k++){
	    	item[k]=new JMenuItem();
	    }
	    
	    int k=0;
	    	
	    for (String i: p.stringPropertyNames()) {
	       item[k].setText(p.getProperty(i));
	       
	        item[k].addActionListener(new ActionListener() {
	        	
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		JMenuItem click= (JMenuItem) e.getSource();
	        		
	        		String clicks= click.getText();
	        		
	        		Invoker invoker = new Invoker();
	        		
	        		if(clicks.equals("eu.telecomnancy.CommandeOn")){
	        			invoker.execute(new CommandOn(sensor));
	        		}
	        		else if(clicks.equals("eu.telecomnancy.CommandeOff")){
	        			invoker.execute(new CommandOff(sensor));
	        		}
	        		else if(clicks.equals("eu.telecomnancy.CommandeGetValue")){
	        			try {
							invoker.execute(new CommandGetValue(sensor));
						} catch (SensorNotActivatedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
	        		}
	        		else if(clicks.equals("eu.telecomnancy.CommandeUpdate")){
	        			invoker.execute(new CommandUpdate(sensor));
	        		}
	        		
	        	}
	        });
	        menu.add(item[k]);
	       k++;
	    }
	    
	    this.add(menu);
	    
	
	
	}

}
