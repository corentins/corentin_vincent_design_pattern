package eu.telecomnancy.ui;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.command.CommandOff;
import eu.telecomnancy.command.CommandOn;
import eu.telecomnancy.command.CommandUpdate;
import eu.telecomnancy.command.Invoker;
import eu.telecomnancy.decorator.Conversion;
import eu.telecomnancy.decorator.Decorator;
import eu.telecomnancy.decorator.Round;
import eu.telecomnancy.observer.IObservateur;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.Menu;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorView extends JPanel implements IObservateur{
    private AbstractSensor sensor;
    private Invoker invoker;
    private CommandOn onCommand;
    private CommandOff offCommand;
    private CommandUpdate updateCommand;

  
   
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton conversion = new JButton("Fahrenheit �F");
    private JButton arrondir = new JButton("Arrondir");

    public SensorView(AbstractSensor c) throws IOException {
        this.sensor = c;
        this.setLayout(new BorderLayout());
    	
        Menu menu = new Menu(sensor);
        this.add(menu,  BorderLayout.WEST);
        onCommand = new CommandOn(sensor);
        offCommand = new CommandOff(sensor);
        updateCommand = new CommandUpdate(sensor);
      
        invoker= new Invoker();

        value.setHorizontalAlignment(SwingConstants.CENTER);
        

        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                invoker.execute(onCommand);
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	invoker.execute(offCommand);
            
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	invoker.execute(updateCommand);
            }
        });
        
         conversion.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Decorator conversion = new Conversion(sensor);
        		AbstractSensor sensor2=sensor;
        		sensor=conversion;
        		try {
        			String temp =String.valueOf(conversion.getValue());
					value.setText(temp+"�F");
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		sensor=sensor2;
        	}
        });
        
        arrondir.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		Decorator round = new Round(sensor);
        		AbstractSensor sensor3=sensor;
        		sensor=round;
        		try {
					update();
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		sensor=sensor3;
        	}
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 4));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(conversion);
        buttonsPanel.add(arrondir);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        
      
    }

	@Override
	public void update() throws SensorNotActivatedException{
		
		String temp =String.valueOf(sensor.getValue());
		value.setText(temp+"�C");	
	}

}
