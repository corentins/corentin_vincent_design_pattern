package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.Menu;

public class MainWindow extends JFrame {

    private AbstractSensor sensor;
    private SensorView sensorView;
    private Menu menu;

    public MainWindow(AbstractSensor sensor) throws IOException {
        this.sensor = sensor;
        
        this.sensorView = new SensorView(this.sensor);
        new Menu(sensor);
        this.setJMenuBar(menu);
        
        sensor.addObservateur(sensorView);
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        
       
        
        this.setVisible(true);
    }


}
