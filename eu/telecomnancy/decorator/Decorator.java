package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class Decorator extends AbstractSensor {
	
	protected AbstractSensor sensor;
	
	public Decorator(AbstractSensor sensor){
		this.sensor=sensor;
	}

	@Override
	public void on() {
		this.sensor.on();
		
	}
	
	@Override
	public void off() {
		this.sensor.off();
		
	}

	@Override
	public boolean getStatus() {
		
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.sensor.update();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.sensor.getValue();
	}


}
