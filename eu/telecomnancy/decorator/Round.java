package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Round extends Decorator{

	public Round(AbstractSensor sensor) {
		super(sensor);
	}
	public double getValue() throws SensorNotActivatedException{
		int round =(int) Math.round(this.sensor.getValue());
		return round;
	}

}
