package eu.telecomnancy.decorator;
import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;



public class Conversion extends Decorator{

	public Conversion(AbstractSensor sensor) {
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException{
		double val = this.sensor.getValue()*1.8+32;
		return val;
	}
}
